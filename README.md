Tezos Docker Run
=========

A simple ansible role to manage a tezos node using docker

Role Variables
--------------

```
rpc_addr: 127.0.0.1:8732
net_addr: 9732
branch: master
network: babylonnet
protocol: "005-PsBabyM1"
history_mode: full
subnet: 172.18.0
start_baker: true
bakers:
  - name: bob
    phk: tz1XBTuhu17722dA2ALQZFf99iyNfksdQdsG
```

Moreover if the variable `snapshot` is define and refers to a valid
url to a tezos snapshot, this is going to be download and used to
initialize the node. Notice that the type of snapshot and the 
`history_mode` mode must be coherent.

The node by default listen on the port 9732 of the host and rpc
are restricted to localhost.

Dependencies
------------

- nomadic-common : a role to initialize a machine to run a tezos node.

Example Playbook
----------------

### Init a babylonnet node with a snapshot

```
- name: init a babylonnet docker node from snapshot
  hosts: nomadic
  roles:
    - role: "tezos-docker-run"
      network: "babylonnet"
      protocol: "005-PsBabyM1"
      snapshot: "https://www.lambsonacid.nl/babylonnet/BMFkaz9mPeALYpnUMATaePJzvg5rTT7RsLP6i7zoELDPNDJJCZv_FULL"
      history_mode: "full"
      init_docker: true
  tags:
    - tezos-babylonnet-init
```

### Start a Tezos node

```
- name: run babylonnet docker node
  hosts: nomadic
  roles:
    - role: "tezos-docker-run"
      network: "babylonnet"
      protocol: "005-PsBabyM1"
      history_mode: "full"
  tags:
    - tezos-babylonnet
```

### Configure the signer

```
- name: configure signer
  hosts: nomadic
  roles:
    - role: "tezos-docker-run"
      network: "babylonnet"
      protocol: "005-PsBabyM1"
      history_mode: "full"
      bakers:
        - name: bob
          phk: tz1XBTuhu17722dA2ALQZFf99iyNfksdQdsG
  tags:
    - tezos-babylonnet-signer
```

### Start baker, endorser and accuser

```
- name: start baker
  hosts: nomadic
  roles:
    - role: "tezos-docker-run"
      network: "babylonnet"
      protocol: "005-PsBabyM1"
      history_mode: "full"
      start_baker: true
  tags:
    - tezos-babylonnet-baker
```

License
-------

AGPL3

Author Information
------------------

Copyright Nomadic Labs
Author Pietro Abate < pietro.abate@nomadic-labs.com>
